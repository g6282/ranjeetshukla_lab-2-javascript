const questionEl = document.querySelector("#question");
const choiceBtns = document.querySelectorAll(".buttons > button");
const choiceSpans = document.querySelectorAll(".buttons > button > span");
const progress = document.querySelector("#progress");

var qno = 0;
var correct = 0;

const questions = [
  {
    statement:
      "Through which of the following States does the river “Chambal” flow?",
    choices: {
      a: "U.P., M.P., Rajasthan",
      b: "M.P., Gujarat, U.P.",
      c: "Rajasthan, M.P., Bihar",
      d: "Gujarat, M.P., U.P.",
    },
    answer: "a",
  },
  {
    statement:
      "Which of the following is the first fully air-conditioned train in India?",
    choices: {
      a: "Bangalore Lal Bagh Express",
      b: "Bangalore Lal Bagh Express",
      c: "Rajdhani Express ",
      d: "Grand trunk Express",
    },
    answer: "c",
  },
  {
    statement: "What was the life expectancy at birth for India for 1991-96?",
    choices: {
      a: "60 years",
      b: "61.6 years",
      c: "62.2 years",
      d: "63 years",
    },
    answer: "c",
  },
  {
    statement: "The tropical cyclones of the Bay of Bengal are usually called",
    choices: {
      a: "Hurricanes",
      b: "Typhoons",
      c: "Depression",
      d: "Tornadoes",
    },
    answer: "c",
  },
  {
    statement: "The longest highway in India runs from",
    choices: {
      a: "Kolkata to jammu",
      b: "Jammu to Kanyakumari",
      c: "Ambala to Nagercoil",
      d: "Varanasi to Kanyakumari",
    },
    answer: "d",
  },
];

window.addEventListener("load", setQuiz);

function setQuiz() {
  questionEl.innerText = "Q" + (qno + 1) + ". " + questions[qno].statement;
  choiceSpans[0].innerText = questions[qno].choices.a;
  choiceSpans[1].innerText = questions[qno].choices.b;
  choiceSpans[2].innerText = questions[qno].choices.c;
  choiceSpans[3].innerText = questions[qno].choices.d;
  progress.innerText = `Question ${qno + 1} of ${questions.length}`;
}



choiceBtns.forEach((element) => {
  element.addEventListener("click", (e) => {
    checkAnswer(e.target.id);
    nextQ();
    if (qno == questions.length) {
      displayScoreCard();
      return;
    }
    setQuiz();
  });
});

function nextQ() {
  if (qno < questions.length) {
    qno += 1;
  }
}

function displayScoreCard() {
  const quizArea = document.querySelector("#quiz");
  const percent = (correct * 100) / questions.length;
  quizArea.innerHTML = `<h1>Result</h1><h2 id="score">Your Score: ${correct} / ${questions.length} <br />Total percentage:( ${percent}% )</h2>`;
}

function checkAnswer(selected) {
  switch (selected) {
    case "btn0":
    case "choice0":
      if (questions[qno].answer == "a") {
        correct += 1;
      }
      break;

    case "btn1":
    case "choice1":
      if (questions[qno].answer == "b") {
        correct += 1;
      }
      break;

    case "btn2":
    case "choice2":
      if (questions[qno].answer == "c") {
        correct += 1;
      }
      break;

    case "btn3":
    case "choice3":
      if (questions[qno].answer == "d") {
        correct += 1;
      }
      break;

    default:
      alert("Error! Please click on the choices only");
  }
}
